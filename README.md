# README#

## **Description:** ##
1. Small Scale bitcoin Miner that prints all hashes with the given number of leading zeros

2. A distributed implementation. Can have Master and worker nodes. Master nodes assign the work and consolidate the reports( mined hash coins) from the worker nodes.

## **Execution:** ##
* Requirements : sbt, scala

* **IMPORTANT** Change the IP address of server and client in the configuration files under src/main/resources. The files are named server_application.conf and local_application.conf

* sbt should take care of all dependencies. build.sbt is included.

* To run the program in *Master* mode:
1. sbt "run <numZeros>" 

2. numZeros indicate the number of leading zeros that are desired in output hash.

3. Master mode starts one worker on the local machine and start mining.
 
* To run the program in *Worker* mode:
1. sbt "run <ipaddress>"

2. ipaddress of the master

3. Make sure the master is running beforehand

4. Mines the coins and reports back to the master. No output on worker.

5. Workers try 100000 random strings before terminating. This is hard coded, can be increased.